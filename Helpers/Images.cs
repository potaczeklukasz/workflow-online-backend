using System;
using System.Drawing;

namespace workflow_online_backend.Helpers
{
    public class Images
    {
        public static void SaveImage(Image image, string fileName)
        {
            var photo = resizeImage(image, 265);
            photo.Save("Gfx/" + fileName);
        }

        public static Image resizeImage(Image image, int size)
        {
            float imageWidth = (float)image.Width, imageHeight = (float)image.Height;
            float width, height;
            float ratio = imageWidth / imageHeight;

            if (ratio <= 1)
            {
                width = size;
                height = size * (imageHeight / imageWidth);
            }
            else
            {
                height = size;
                width = size * ratio;
            }

            return (Image)(new System.Drawing.Bitmap(image, new Size(Convert.ToInt32(width), Convert.ToInt32(height))));
        }
    }
}