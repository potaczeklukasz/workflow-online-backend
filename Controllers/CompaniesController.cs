using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using workflow_online_backend.Data;
using workflow_online_backend.DTO;

namespace workflow_online_backend.Controllers
{
    [Authorize(Policy = "ApiUser")]
    [Route("api/[controller]")]
    [ApiController]
    public class CompaniesController : Controller
    {
        private readonly ApplicationDbContext _applicationDbContext;

        public CompaniesController(ApplicationDbContext applicationDbContext)
        {
            _applicationDbContext = applicationDbContext;
        }

        // Get company details
        [HttpGet("{id}")]
        public async Task<IActionResult> Get(string id)
        {
            var value = await _applicationDbContext.Companies.FirstOrDefaultAsync(x => x.Id == id);
            if (value == null) return BadRequest(new { error = "Company not found" });

            return new OkObjectResult(value);
        }

        // Edit company details
        [HttpPut("edit")]
        public async Task<IActionResult> Put([FromBody]CompanyDto companyDto)
        {
            var result = await _applicationDbContext.Companies.SingleOrDefaultAsync(x => x.Id == companyDto.Id);
            result.CompanyName = companyDto.CompanyName;
            result.Email = companyDto.Email;
            result.Address = companyDto.Address;
            result.NIP = companyDto.NIP;
            await _applicationDbContext.SaveChangesAsync();

            return new NoContentResult();
        }
    }
}