using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using workflow_online_backend.Data;
using workflow_online_backend.DTO;
using workflow_online_backend.Entities;

namespace workflow_online_backend.Controllers
{
    [Authorize(Policy = "ApiUser")]
    [Route("api/[controller]")]
    [ApiController]
    public class ModulesController : Controller
    {
        private readonly ApplicationDbContext _applicationDbContext;

        public ModulesController(ApplicationDbContext applicationDbContext)
        {
            _applicationDbContext = applicationDbContext;
        }

        // Get all modules and information which were purchased by company
        [HttpGet("{id}")]
        public async Task<IActionResult> Get(string id)
        {
            var modules = await _applicationDbContext.Modules.ToListAsync();
            var purchasedModules = await _applicationDbContext.PurchasedModules
                .Where(x => x.CompanyId == id)
                .ToListAsync();

            return new OkObjectResult(new { modules, purchasedModules });
        }

        // Assign purchased module to the company
        [HttpPost]
        public async Task<IActionResult> Post([FromBody]PurchasedModuleDto purchasedModuleDto)
        {
            string Id = Guid.NewGuid().ToString();
            await _applicationDbContext.PurchasedModules.AddAsync(new PurchasedModule
            {
                Id = Id,
                ModuleId = purchasedModuleDto.ModuleId,
                CompanyId = purchasedModuleDto.CompanyId
            });
            await _applicationDbContext.SaveChangesAsync();

            return new OkObjectResult(new { Id });
        }

        // Deactivate module
        [HttpDelete]
        public async Task<IActionResult> Delete([FromBody]PurchasedModuleDto purchasedModuleDto)
        {
            var result = await _applicationDbContext.PurchasedModules.FirstOrDefaultAsync(x => x.Id == purchasedModuleDto.Id);
            if (result != null)
            {
                _applicationDbContext.PurchasedModules.Remove(result);
                _applicationDbContext.SaveChanges();
            }

            return new NoContentResult();
        }
    }
}