using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using workflow_online_backend.Data;
using workflow_online_backend.DTO;
using workflow_online_backend.Entities;
using workflow_online_backend.Helpers;

namespace workflow_online_backend.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AccountsController : Controller
    {
        private readonly UserManager<IdentityUser> _userManager;
        private readonly IMapper _mapper;
        private readonly ApplicationDbContext _applicationDbContext;

        public AccountsController(UserManager<IdentityUser> userManager, IMapper mapper, ApplicationDbContext applicationDbContext)
        {
            _mapper = mapper;
            _userManager = userManager;
            _applicationDbContext = applicationDbContext;
        }

        private async void AddUserToDatabase(IdentityUser userIdentity, RegistrationUserDto registrationUserDto, string companyId)
        {
            await _applicationDbContext.Users.AddAsync(new User
            {
                Id = userIdentity.Id,
                FirstName = registrationUserDto.FirstName,
                LastName = registrationUserDto.LastName,
                CompanyId = companyId,
                Position = registrationUserDto.Position,
                Email = registrationUserDto.Email
            });
        }

        // Register new account
        [HttpPost("register")]
        public async Task<IActionResult> Register([FromBody]RegistrationUserDto registrationUserDto)
        {
            if (!ModelState.IsValid) return BadRequest(ModelState);

            // Create new User in Identity
            var userIdentity = _mapper.Map<IdentityUser>(registrationUserDto);
            var result = await _userManager.CreateAsync(userIdentity, registrationUserDto.Password);
            if (!result.Succeeded) return new BadRequestObjectResult(Errors.AddErrorsToModelState(result, ModelState));

            if (registrationUserDto.CompanyId != null) // add new User to existing company
            {
                // Add User details in User table in database
                AddUserToDatabase(userIdentity, registrationUserDto, registrationUserDto.CompanyId);
                await _applicationDbContext.SaveChangesAsync();
            }
            else // add new Company to Database and add User assigned to created company
            {
                string companyId = Guid.NewGuid().ToString(); // generate Company Id

                // Create new Company
                await _applicationDbContext.Companies.AddAsync(new Company
                {
                    Id = companyId,
                    CompanyName = registrationUserDto.CompanyName,
                    Email = registrationUserDto.CompanyEmail,
                    NIP = registrationUserDto.CompanyNIP,
                    Address = registrationUserDto.CompanyAddress,
                });

                // Add User details in User table in database
                AddUserToDatabase(userIdentity, registrationUserDto, companyId);
                await _applicationDbContext.SaveChangesAsync();
            }

            return Created("/accounts", null);
        }

        // Add new password change request to database and send email with links to reset the password
        [HttpGet("password/{email}")]
        public async Task<IActionResult> AddNewChangePasswordRequest(string email)
        {
            var user = await _applicationDbContext.Users.FirstOrDefaultAsync(x => x.Email == email);
            if (user == null) return BadRequest(new { error = "User not found" });

            // Add new password change request to database
            string id = Guid.NewGuid().ToString();
            await _applicationDbContext.ChangePasswordRequests.AddAsync(new ChangePassword
            {
                Id = id,
                UserId = user.Id,
                UserEmail = user.Email,
                RequestDate = DateTime.Now
            });
            await _applicationDbContext.SaveChangesAsync();

            // Send email with links
            try
            {
                MailMessage mailMessage = new MailMessage("workflowonlinepass@gmail.com", email);
                mailMessage.Body = "Link to reset password: http://localhost:3000/password/reset/?id=" + id + "\n If it wasn't you, follow this link: http://localhost:3000/password/cancel/?id=" + id;
                mailMessage.Subject = "subject";

                SmtpClient client = new SmtpClient("smtp.gmail.com", 587);
                client.Credentials = new NetworkCredential("workflowonlinepass@gmail.com", "workflowOnline!");
                client.EnableSsl = true;
                client.Send(mailMessage);
            }
            catch (Exception ex)
            {
                return BadRequest(new { error = "Sending email failed", Message = ex.Message });
            }

            return Created("/accounts/password", null);
        }

        // Check if link to reset password is valid and still active
        [HttpGet("password/reset/{id}")]
        public async Task<IActionResult> CheckChangePasswordRequest(string id)
        {
            var request = await _applicationDbContext.ChangePasswordRequests.FirstOrDefaultAsync(x => x.Id == id);
            if (request == null) return BadRequest(new { error = "The link is invalid" });

            if ((DateTime.Now - request.RequestDate).TotalHours <= 24) return Ok();
            else return BadRequest();
        }

        // Delete password change request ("It's not me" option)
        [HttpGet("password/cancel/{id}")]
        public async Task<IActionResult> CancelChangePasswordRequest(string id)
        {
            var request = await _applicationDbContext.ChangePasswordRequests.FirstOrDefaultAsync(x => x.Id == id);
            if (request == null) return BadRequest(new { error = "Password change request is already deleted" });
            _applicationDbContext.ChangePasswordRequests.Remove(request);
            await _applicationDbContext.SaveChangesAsync();
            return Ok();
        }

        // Change user password
        [HttpPost("password/new")]
        public async Task<IActionResult> ChangePassword([FromBody]ChangePasswordDto changePasswordDto)
        {
            var changeRequest = await _applicationDbContext.ChangePasswordRequests.FirstOrDefaultAsync(x => x.Id == changePasswordDto.Id);
            if (changeRequest == null) return BadRequest(new { error = "Change password request was not found" });

            // Search for user
            IdentityUser identityUser = await _userManager.FindByIdAsync(changeRequest.UserId);
            if (identityUser == null) return BadRequest(new { error = "User assigned to this change password request is no longer exist" });

            // password change
            identityUser.PasswordHash = _userManager.PasswordHasher.HashPassword(identityUser, changePasswordDto.Password);
            var result = await _userManager.UpdateAsync(identityUser);

            // Delete password change request
            _applicationDbContext.ChangePasswordRequests.Remove(changeRequest);
            await _applicationDbContext.SaveChangesAsync();
            return Ok();
        }
    }
}
