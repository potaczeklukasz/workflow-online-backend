using System;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using workflow_online_backend.Data;
using workflow_online_backend.DTO;

namespace workflow_online_backend.Controllers
{
    [Authorize(Policy = "ApiUser")]
    [Route("api/[controller]")]
    [ApiController]
    public class UsersController : Controller
    {
        private readonly ApplicationDbContext _applicationDbContext;
        private readonly UserManager<IdentityUser> _userManager;

        public UsersController(ApplicationDbContext applicationDbContext, UserManager<IdentityUser> userManager)
        {
            _applicationDbContext = applicationDbContext;
            _userManager = userManager;
        }

        private async Task<SearchInfoDto> GetInfo(SearchUsersDto searchUsersDto)
        {
            int records = await _applicationDbContext.Users
                .Where(x => x.CompanyId == searchUsersDto.companyId)
                .Where(x => x.FirstName.ToLower().Contains(searchUsersDto.FirstName.ToLower()))
                .Where(x => x.LastName.ToLower().Contains(searchUsersDto.LastName.ToLower()))
                .Where(x => x.Position.ToLower().Contains(searchUsersDto.Position.ToLower()))
                .CountAsync();

            SearchInfoDto searchInfoDto = new SearchInfoDto
            {
                Records = records,
                Pages = Math.Ceiling((records / 5d))
            };

            return await Task<SearchInfoDto>.FromResult(searchInfoDto);
        }

        // Return results of searching in Users database (if searching is empty - return all users assign to company)
        // and informations about amount of users assign to company, number of pages and current selected page
        [HttpPost]
        public async Task<IActionResult> Post([FromBody]SearchUsersDto searchUsersDto)
        {
            // Get info about records number and pages number
            var info = await GetInfo(searchUsersDto);
            info.CurrentPage = searchUsersDto.Page;

            // If the page provided by the user is higher than the number of available pages
            // set the current page as the last available page
            if (info.Pages < searchUsersDto.Page) info.CurrentPage = info.Pages;
            else if (searchUsersDto.Page <= 0) info.CurrentPage = 1;

            // Get searching results
            var values = await _applicationDbContext.Users
                .Where(x => x.CompanyId == searchUsersDto.companyId)
                .Where(x => x.FirstName.ToLower().Contains(searchUsersDto.FirstName.ToLower()))
                .Where(x => x.LastName.ToLower().Contains(searchUsersDto.LastName.ToLower()))
                .Where(x => x.Position.ToLower().Contains(searchUsersDto.Position.ToLower()))
                .OrderBy(x => x.LastName)
                .ThenBy(x => x.FirstName)
                .Skip(Convert.ToInt32((info.CurrentPage - 1) * 5))
                .Take(5)
                .ToListAsync();

            return new OkObjectResult(new { info, values });
        }

        // Get information about specified user
        [HttpGet("user/{id}")]
        public async Task<IActionResult> Get(string id)
        {
            var value = await _applicationDbContext.Users.FirstOrDefaultAsync(x => x.Id == id);
            if (value == null) return BadRequest(new { error = "User not found" });

            return new OkObjectResult(value);
        }

        // Update information about user
        [HttpPut]
        public async Task<IActionResult> Put([FromBody]UserDto userDto)
        {
            var result = await _applicationDbContext.Users.SingleOrDefaultAsync(x => x.Id == userDto.Id);
            result.FirstName = userDto.FirstName;
            result.LastName = userDto.LastName;
            result.Position = userDto.Position;
            result.Email = userDto.Email;
            await _applicationDbContext.SaveChangesAsync();

            return new NoContentResult();
        }

        // Delete user
        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(string id)
        {
            var identityUser = await _userManager.FindByIdAsync(id);
            if (identityUser == null) return BadRequest(new { error = "User not found" });

            await _userManager.DeleteAsync(identityUser);

            var user = await _applicationDbContext.Users.Where(x => x.Id == id).SingleOrDefaultAsync();

            // delete user photo
            FileInfo file = new FileInfo("Gfx/" + user.PictureUrl);
            if (file.Exists) file.Delete();

            if (user != null)
            {
                _applicationDbContext.Users.Remove(user);
                _applicationDbContext.SaveChanges();
            }

            return new NoContentResult();
        }
    }
}