using System;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using workflow_online_backend.Data;
using workflow_online_backend.DTO;
using workflow_online_backend.Entities;
using workflow_online_backend.Helpers;

namespace workflow_online_backend.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ImagesController : Controller
    {
        private readonly ApplicationDbContext _applicationDbContext;

        public ImagesController(ApplicationDbContext applicationDbContext)
        {
            _applicationDbContext = applicationDbContext;
        }

        // Send required image
        [HttpGet("{name}")]
        public async Task<IActionResult> Get(string name)
        {
            var image = System.IO.File.OpenRead("Gfx/" + name);
            return await Task.Run(() => File(image, "image/jpeg"));
        }

        // Add new user photo
        [Authorize(Policy = "ApiUser")]
        [HttpPost("upload")]
        public async Task<IActionResult> Post([FromForm]FileDto fileDto)
        {
            if (fileDto.File == null || fileDto.File.Length == 0) return BadRequest(new { Error = "file not selected" });

            Image image = Image.FromStream(fileDto.File.OpenReadStream(), true, true);

            string fileName = Guid.NewGuid().ToString() + ".jpg";
            Images.SaveImage(image, fileName);

            var user = await _applicationDbContext.Users.FirstOrDefaultAsync(x => x.Id == fileDto.Id);
            if (user == null) return BadRequest(new { error = "User not found" });

            // delete previous photo
            FileInfo file = new FileInfo("Gfx/" + user.PictureUrl);
            if (file.Exists) file.Delete();

            user.PictureUrl = fileName;
            await _applicationDbContext.SaveChangesAsync();

            return new OkObjectResult(new { fileName = fileName });
        }
    }
}