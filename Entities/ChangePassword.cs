using System;

namespace workflow_online_backend.Entities
{
    public class ChangePassword
    {
        public string Id { get; set; }
        public string UserId { get; set; }
        public string UserEmail { get; set; }
        public DateTime RequestDate { get; set; }
    }
}