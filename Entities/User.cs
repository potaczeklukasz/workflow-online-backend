namespace workflow_online_backend.Entities
{
    public class User
    {
        public string Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string PictureUrl { get; set; }
        public string CompanyId { get; set; }
        public string Position { get; set; }
        public string Email { get; set; }
    }
}