namespace workflow_online_backend.Entities
{
    public class PurchasedModule
    {
        public string Id { get; set; }
        public string ModuleId { get; set; }
        public string CompanyId { get; set; }
    }
}