namespace workflow_online_backend.Entities
{
    public class Company
    {
        public string Id { get; set; }
        public string CompanyName { get; set; }
        public string Email { get; set; }
        public string Address { get; set; }
        public string NIP { get; set; }
    }
}