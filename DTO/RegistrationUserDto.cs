namespace workflow_online_backend.DTO
{
    public class RegistrationUserDto
    {
        public string Email { get; set; }
        public string Password { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Position { get; set; }

        public string CompanyName { get; set; }
        public string CompanyId { get; set; }
        public string CompanyNIP { get; set; }
        public string CompanyAddress { get; set; }
        public string CompanyEmail { get; set; }
    }
}