namespace workflow_online_backend.DTO
{
    public class CredentialsDto
    {
        public string UserName { get; set; }
        public string Password { get; set; }
    }
}