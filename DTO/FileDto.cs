using Microsoft.AspNetCore.Http;

namespace workflow_online_backend.DTO
{
    public class FileDto
    {
        public IFormFile File { get; set; }
        public string Id { get; set; }
    }
}