namespace workflow_online_backend.DTO
{
    public class PurchasedModuleDto
    {
        public string Id { get; set; }
        public string ModuleId { get; set; }
        public string CompanyId { get; set; }
    }
}