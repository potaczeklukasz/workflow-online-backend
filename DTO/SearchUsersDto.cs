namespace workflow_online_backend.DTO
{
    public class SearchUsersDto
    {
        public string companyId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Position { get; set; }
        public int Page { get; set; }
    }
}