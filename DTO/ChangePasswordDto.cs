namespace workflow_online_backend.DTO
{
    public class ChangePasswordDto
    {
        public string Id { get; set; }
        public string Password { get; set; }
    }
}