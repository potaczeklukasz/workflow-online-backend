namespace workflow_online_backend.DTO
{
    public class SearchInfoDto
    {
        public int Records { get; set; }
        public double Pages { get; set; }
        public double CurrentPage { get; set; }
    }
}