using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using workflow_online_backend.Entities;

namespace workflow_online_backend.Data
{
    public class ApplicationDbContext : IdentityDbContext<IdentityUser>
    {
        public ApplicationDbContext(DbContextOptions options) : base(options)
        {
        }

        public DbSet<IdentityUser> IdentityUser { get; set; }
        public new DbSet<User> Users { get; set; }
        public DbSet<Company> Companies { get; set; }
        public DbSet<Module> Modules { get; set; }
        public DbSet<PurchasedModule> PurchasedModules { get; set; }
        public DbSet<ChangePassword> ChangePasswordRequests { get; set; }
    }
}