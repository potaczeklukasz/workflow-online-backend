using AutoMapper;
using Microsoft.AspNetCore.Identity;
using workflow_online_backend.DTO;
using workflow_online_backend.Entities;

namespace AngularASPNETCore2WebApiAuth.ViewModels.Mappings
{
    public class ViewModelToEntityMappingProfile : Profile
    {
        public ViewModelToEntityMappingProfile()
        {
            CreateMap<RegistrationUserDto, IdentityUser>().ForMember(au => au.UserName, map => map.MapFrom(vm => vm.Email));
        }
    }
}
